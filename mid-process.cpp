#include <iostream>
#include <unistd.h> // for syscalls
#include <sys/wait.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <sys/stat.h>
#include <math.h>
#include <fcntl.h>

using namespace std;

#define BUF_SIZE 1024
#define R_END 0
#define W_END 1
#define LEAF_PROCESS_PATH "./leaf-process.out"
#define NPIPE_PATH "_np2_"
#define CHILD_RET_LEN 12

vector<string> split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while (getline(ss, tok, delimiter)) {
        if (tok != "")
            internal.push_back(tok);
    }

    return internal;
}

vector<int> stringToVector(string s) {
    vector<string> strLine;
    vector<int> intLine;

    strLine = split(s, ',');
    for (int i = 0; i < strLine.size(); i++) {
        intLine.push_back(stoi(strLine[i]));
    }
    return intLine;
}

string vectorToString(vector<int> v) {
    string s;
    for(int i=0; i < v.size(); i++) {
        s += to_string(v[i]);
        if(i != v.size() - 1)
            s += ",";
    }
    return s;
}

bool doesFileExist(string path) {
    fstream stream(path);
    if (!stream)
        return false;
    return true;
}

double calcEuclideanDist(vector<int> a, vector<int> b) {
    double dist = 0;
    for(int i=0; i<a.size(); i++) {
        dist += (a[i] - b[i]) * (a[i] - b[i]);
    }
    return sqrt(dist);
}

vector<int> chooseClosest(vector<int> src, vector<vector<int> > samples, int* fileID) {
    double minDist = INFINITY;
    int closestIndex;
    double holder;

    for(int i=0; i<samples.size(); i++) {
        if((holder = calcEuclideanDist(src, samples[i])) < minDist) {
            minDist = holder;
            closestIndex = i;
        }
    }

    *fileID = closestIndex+1;
    return samples[closestIndex];
}

vector<int> getChildResults(string path) {
    char buf[BUF_SIZE];
    vector<string> childResStr;

    int fifoFD;
    if((fifoFD = open(path.c_str(), O_RDONLY)) < 0) {
        mkfifo(path.c_str(), 0666);
        fifoFD = open(path.c_str(), O_RDONLY);
    }
    int holder = read(fifoFD, buf, BUF_SIZE);
    close(fifoFD);
    childResStr = split(buf, '*');

    return stringToVector(childResStr[0]);
}

int main(int argc, char** argv) {
    char buf[BUF_SIZE];

    read(atoi(argv[1]), buf, BUF_SIZE);
    vector<string> inherited = split(buf, '*');

    // cout << "mid-process with pid " << getpid() << " | inherited[0]=" <<
    //         inherited[0] << " | inherited[1]=" << inherited[1] <<
    //         " | inherited[2]=" << inherited[2] << endl;

    int userFileCounter = 1;
    while(doesFileExist(inherited[1] + "/users-" + to_string(userFileCounter) + ".csv")) {
        int p[2];
        if(pipe(p) < 0) {
            cout<<"unnamed pipe error!\n";
            exit(EXIT_FAILURE);
        }
        int n = fork();

        if(n == 0) {    // child
            char *args[] = {LEAF_PROCESS_PATH, (char*)to_string(p[R_END]).c_str(), NULL};
            if(execv(args[0], args) < 0) {
                cout<<"exec failed\n";
                exit(EXIT_FAILURE);
            }

        }
        else if(n > 0) {    // parent
            string msg = inherited[0] + "*" +                                                   // traits file line
                         inherited[1] + "/users-" + to_string(userFileCounter) + ".csv" + "*" + // user file path
                         inherited[2] + NPIPE_PATH + to_string(userFileCounter);                // named pipe name
            if(write(p[W_END], msg.c_str(), msg.length()+1) < 0) {
                cout<<"writing to pipe failed!\n";
                exit(EXIT_FAILURE);
            }

        }
        else { // error
            cout<<"fork error\n";
            exit(EXIT_FAILURE);
        }
        userFileCounter++;
    }
    userFileCounter--;


    vector<vector<int>> samples;
    for(int i=0; i<userFileCounter; i++) {
        samples.push_back(getChildResults(inherited[2] + NPIPE_PATH + to_string(i + 1)));
    }

    vector<int> src = stringToVector(inherited[0]);
    int fileID;
    vector<int> closestSample = chooseClosest(src, samples, &fileID);

    // cout<<"*********************************"<<endl;
    // for(int i=0; i<samples.size(); i++) {
    //     for (int j = 0; j < samples[i].size(); j++) {
    //         cout << samples[i][j] << '\t';
    //     }
    //     cout << endl;
    // }
    // cout << "*********************************" << endl;
    // cout<<"file id= "<<fileID<<endl;

    string msg = vectorToString(closestSample) + "*" + to_string(fileID);

    int fifoFD;
    if((fifoFD = open(inherited[2].c_str(), O_WRONLY)) < 0) {
        mkfifo(inherited[2].c_str(), 0666);
        fifoFD = open(inherited[2].c_str(), O_WRONLY);
    }

    if(write(fifoFD, msg.c_str(), msg.length() + 1) < 0) {
        cout << "write to named pipe failed\n";
        exit(EXIT_FAILURE);
    }
    close(fifoFD);

    exit(EXIT_SUCCESS);
}