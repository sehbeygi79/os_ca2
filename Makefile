all: main-process.out mid-process.out leaf-process.out

main-process.out: main-process.cpp
	g++ -std=c++11 main-process.cpp -o main-process.out

mid-process.out: mid-process.cpp
	g++ -std=c++11 mid-process.cpp -o mid-process.out

leaf-process.out: leaf-process.cpp
	g++ -std=c++11 leaf-process.cpp -o leaf-process.out

.PHONY: clean
clean:
	rm *.o