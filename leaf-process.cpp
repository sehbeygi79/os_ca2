#include <iostream>
#include <unistd.h> // for syscalls
#include <sys/wait.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <fcntl.h>
#include <sys/stat.h>
#include <math.h>
using namespace std;

#define BUF_SIZE 1024
#define R_END 0
#define W_END 1

vector<string> split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while (getline(ss, tok, delimiter)) {
        if (tok != "")
            internal.push_back(tok);
    }

    return internal;
}

vector<vector<int> > getInputs(string path) {
    fstream stream(path);
    if (!stream) {
        cout << path << " not found" << endl;
        exit(EXIT_FAILURE);
    }
    string line;
    vector<vector<int>> outputFile;

    while (getline(stream, line)) {
        vector<string> strLine;
        vector<int> intLine;

        strLine = split(line, ',');
        for (int i = 0; i < strLine.size(); i++) {
            intLine.push_back(stoi(strLine[i]));
        }
        outputFile.push_back(intLine);
    }

    return outputFile;
}

vector<int> stringToVector(string s) {
    vector<string> strLine;
    vector<int> intLine;

    strLine = split(s, ',');
    for (int i = 0; i < strLine.size(); i++) {
        intLine.push_back(stoi(strLine[i]));
    }
    return intLine;
}

string vectorToString(vector<int> v) {
    string s;
    for(int i=0; i < v.size(); i++) {
        s += to_string(v[i]);
        if(i != v.size() - 1)
            s += ",";
    }
    return s;
}

string getUserFileID(string path) {
    string id;
    int counter = 0;
    while(true) {
        if(path[path.length() - 5 - counter] != '-')
            id = path[path.length() - 5 - counter] + id;
        else
            break;
        
        counter++;
    }
    return id;
}

double calcEuclideanDist(vector<int> a, vector<int> b) {
    double dist = 0;
    for(int i=0; i<a.size(); i++) {
        dist += (a[i] - b[i]) * (a[i] - b[i]);
    }
    return sqrt(dist);
}

vector<int> chooseClosest(vector<int> src, vector<vector<int> > samples) {
    double minDist = INFINITY;
    int closestIndex;
    double holder;

    for(int i=0; i<samples.size(); i++) {
        if((holder = calcEuclideanDist(src, samples[i])) < minDist) {
            minDist = holder;
            closestIndex = i;
        }
    }

    return samples[closestIndex];
}

int main(int argc, char **argv) {
    char buf[BUF_SIZE];

    read(atoi(argv[1]), buf, BUF_SIZE);
    vector<string> inherited = split(buf, '*');

    // cout << "leaf-process with pid " << getpid() << " | inherited[0]=" <<
    //          inherited[0] << " | inherited[1]=" << inherited[1] << " | inherited[2]=" << inherited[2] << endl;

    vector<vector<int> > samples = getInputs(inherited[1]);
    vector<int> src = stringToVector(inherited[0]);
    vector<int> closestSample = chooseClosest(src, samples);

    // cout << "closest result of file " << inherited[1] << " = " << vectorToString(closestSample) << endl;

    string msg = vectorToString(closestSample) + "*" + getUserFileID(inherited[1]);

    int fifoFD;
    if((fifoFD = open(inherited[2].c_str(), O_WRONLY)) < 0) {
        mkfifo(inherited[2].c_str(), 0666);
        fifoFD = open(inherited[2].c_str(), O_WRONLY);
    }

    if(write(fifoFD, msg.c_str(), msg.length() + 1) < 0) {
        cout << "write to named pipe failed\n";
        exit(EXIT_FAILURE);
    }
    close(fifoFD);
    exit(EXIT_SUCCESS);
}