#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <unistd.h> // for syscalls
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

#define R_END 0
#define W_END 1
#define BUF_SIZE 1024
#define MID_PROCESS_PATH "./mid-process.out"
#define NPIPE_PATH "./np1_"
#define RESULT_PATH "results.csv"

vector<string> extractFile(string path) {

    fstream stream(path);
    if(!stream)
        return {};

    string line;
    vector<string> outputFile;

    while (getline(stream, line)) {
        outputFile.push_back(line);
    }
    return outputFile;
}

void printMatrix(vector<vector<int> > matrix) {
    for(int i=0; i<matrix.size(); i++) {
        for(int j=0; j< matrix[0].size(); j++) {
            cout<<matrix[i][j]<<"\t";
        }
        cout<<'\n';
    }
}

vector<string> split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while (getline(ss, tok, delimiter)) {
        if (tok != "")
            internal.push_back(tok);
    }

    return internal;
}

vector<int> stringToVector(string s) {
    vector<string> strLine;
    vector<int> intLine;

    strLine = split(s, ',');
    for (int i = 0; i < strLine.size(); i++) {
        intLine.push_back(stoi(strLine[i]));
    }
    return intLine;
}

void printChildResults(ofstream* outStream, vector<int> results, int userID, bool lastLine) {

    *outStream<<"users-"<<userID<<',';
    for(int i=0; i<results.size(); i++) {
        *outStream<<results[i];
        if(i != results.size() - 1) {
            *outStream<<',';
        }
    }
    if(!lastLine)
        *outStream<<endl;
}

vector<int> getChildResults(string path, int* userID) {
    char buf[BUF_SIZE];
    vector<string> childResStr;

    int fifoFD;
    if((fifoFD = open(path.c_str(), O_RDONLY)) < 0) {
        mkfifo(path.c_str(), 0666);
        fifoFD = open(path.c_str(), O_RDONLY);
    }
    read(fifoFD, buf, BUF_SIZE);
    close(fifoFD);
    childResStr = split(buf, '*');
    *userID = stoi(childResStr[1]);

    return stringToVector(childResStr[0]);
}

int main(int argc, char** argv) {
    if(argc != 3) {
        cout<<"bad arguments\n";
        exit(EXIT_FAILURE);
    }
    string traitsPath = argv[1];
    string usersPath = argv[2];

    vector<string> traits = extractFile(traitsPath);
    if(traits.empty()) {
        cout << "not found " << traitsPath << "\n";
        exit(EXIT_FAILURE);
    }
    vector<vector<int> > closestSamples;

    for(int i=0; i<traits.size(); i++) {
        int p[2];
        if(pipe(p) < 0) {
            cout<<"unnamed pipe error!\n";
            exit(EXIT_FAILURE);
        }
        int n = fork();

        if(n == 0) {    // child
            char *args[] = {MID_PROCESS_PATH, (char*)to_string(p[R_END]).c_str(), NULL};
            if(execv(args[0], args) < 0) {
                cout<<"exec failed\n";
                exit(EXIT_FAILURE);
            }

        }
        else if(n > 0) {    // parent
            string msg = traits[i] + "*" +              // traits file line
                         usersPath + "*" +              // user file path
                         NPIPE_PATH + to_string(i + 1); // named pipe name

            if(write(p[W_END], msg.c_str(), msg.length()+1) < 0) {
                cout<<"writing to pipe failed!\n";
                exit(EXIT_FAILURE);
            }
        }
        else { // error
            cout<<"fork error\n";
            exit(EXIT_FAILURE);
        }
    }

    ofstream outStream;
    outStream.open(RESULT_PATH);
    if(!outStream) {
        cout << "printing results to file failed\n";
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<traits.size(); i++) {
        int userID;
        vector<int> childResults = getChildResults(NPIPE_PATH + to_string(i + 1), &userID);
        printChildResults(&outStream, childResults, userID, i == (traits.size() - 1));
    }
    outStream.close();
    
    exit(EXIT_SUCCESS);
}

